package com.playwing.exchange.auction.rtb.request;

import com.playwing.exchange.auction.rtb.AbstractRTBObject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Regs extends AbstractRTBObject{

    private int coppa;

}
