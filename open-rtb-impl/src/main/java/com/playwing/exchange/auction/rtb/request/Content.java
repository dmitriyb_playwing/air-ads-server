package com.playwing.exchange.auction.rtb.request;

import com.playwing.exchange.auction.rtb.AbstractRTBObject;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Content extends AbstractRTBObject{

    private String id;

    private Integer episode;

    private String title;

    private String series;

    private String season;

    private String artist;

    private String genre;

    private String album;

    private String isrc;

    private Producer producer;

    private String url;

    private String[] cat;

    private Integer prodq;

    private Integer context;

    private String contentRating;

    private String userRating;

    private Integer qagMediaRating;

    private String keywords;

    private Integer liveStream;

    private Integer sourceRelationship;

    private Integer len;

    private String language;

    private Integer embeddable;

    private List<Data> data;

    @Deprecated
    private Integer videoQuality;
}
