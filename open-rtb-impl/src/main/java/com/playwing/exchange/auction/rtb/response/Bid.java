package com.playwing.exchange.auction.rtb.response;

import com.playwing.exchange.auction.rtb.AbstractRTBObject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Bid extends AbstractRTBObject{

    private String id;

    private String impId;

    private Double price;

    private String nurl;

    private String burl;

    private String lurl;

    private String adm;

    private String adId;

    private String[] aDomain;

    private String bundle;

    private String iUrl;

    private String cid;

    private String crid;

    private String tactic;

    private String[] cat;

    private Integer[] attr;

    private Integer api;

    private Integer protocol;

    private Integer qagMediaRating;

    private String language;

    private String dealId;

    private Integer h;

    private Integer w;

    private Integer wRatio;

    private Integer hRatio;

    private Integer exp;


}
