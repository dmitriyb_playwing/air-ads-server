package com.playwing.exchange.auction.rtb.request.imp;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NativeAd extends AbstractRTBAdFormat {

    private String request;

    private String ver;
}
