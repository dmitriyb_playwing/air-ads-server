package com.playwing.exchange.auction.rtb.request.imp;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Video extends AbstractDurableRTBFormat {

    private Integer w;

    private Integer h;

    private Integer placement;

    private Integer linearity;

    private Integer skip;

    private int skipMin;

    private int skipAfter;

    private int boxingAllowed = 1;

    private Integer[] playbackMethod;

    private Integer playbackEnd;

    private Integer pos;

    @Deprecated
    private Integer protocol;
}
