package com.playwing.exchange.auction.rtb.response;

import com.playwing.exchange.auction.rtb.AbstractRTBObject;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SeatBid extends AbstractRTBObject{

    private List<Bid> bidList;

    private String seat;

    private int group;

}
