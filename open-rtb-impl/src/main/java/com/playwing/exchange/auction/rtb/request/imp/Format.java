package com.playwing.exchange.auction.rtb.request.imp;

import com.playwing.exchange.auction.rtb.AbstractRTBObject;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Format extends AbstractRTBObject{

    private Integer w;

    private Integer h;

    private Integer wRatio;

    private Integer hRatio;

    private Integer wMin;
}
