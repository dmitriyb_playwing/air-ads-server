package com.playwing.exchange.auction.rtb.request.imp;

import com.playwing.exchange.auction.rtb.AbstractRTBObject;
import com.playwing.exchange.auction.rtb.request.Ext;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Metric extends AbstractRTBObject{

    private String type;

    private Double value;

    private String vendor;

}
