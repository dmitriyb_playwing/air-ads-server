package com.playwing.exchange.auction.rtb.request;

import com.playwing.exchange.auction.rtb.AbstractRTBObject;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class User extends AbstractRTBObject{

    private String id;

    private String buyeruid;

    private Integer yob;

    private String gender;

    private String keywords;

    private String customData;

    private Geo geo;

    private List<Data> dataList;

}
