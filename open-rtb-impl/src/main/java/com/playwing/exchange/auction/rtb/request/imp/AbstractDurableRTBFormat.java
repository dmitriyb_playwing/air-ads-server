package com.playwing.exchange.auction.rtb.request.imp;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public abstract class AbstractDurableRTBFormat extends AbstractRTBAdFormat {

    private String[] mimes;

    private Integer minDuration;

    private Integer maxDuration;

    private Integer[] protocols;

    private Integer startDelay;

    private Integer sequence;

    private Integer minBitrate;

    private Integer maxBitrate;

    private Integer[] delivery;

    private List<Banner> companionAd;

    private Integer[] companionType;

    private Integer maxExtended;

}
