package com.playwing.exchange.auction.rtb.request;

import com.playwing.exchange.auction.rtb.AbstractRTBObject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class App extends AbstractRTBObject{

    private String id;

    private String name;

    private String bundle;

    private String domain;

    private String storeUrl;

    private String[] cat;

    private String[] sectionCat;

    private String[] pageCat;

    private String ver;

    private Integer privacyPolicy;

    private Integer paid;

    private Publisher publisher;

    private Content content;

    private String keywords;
}
