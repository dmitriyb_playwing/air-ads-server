package com.playwing.exchange.auction.rtb.request.imp;

import com.playwing.exchange.auction.rtb.AbstractRTBObject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class AbstractRTBAdFormat extends AbstractRTBObject {

    private Integer[] battr;

    private Integer[] api;
}
