package com.playwing.exchange.auction.rtb.request;

import com.playwing.exchange.auction.rtb.AbstractRTBObject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Source extends AbstractRTBObject{

    private int fd;

    private String tid;

    private String pchain;
}
