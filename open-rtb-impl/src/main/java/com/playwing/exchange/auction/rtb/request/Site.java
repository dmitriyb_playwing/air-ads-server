package com.playwing.exchange.auction.rtb.request;

import com.playwing.exchange.auction.rtb.AbstractRTBObject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Site extends AbstractRTBObject{

    private String id;

    private String name;

    private String domain;

    private String[] cat;

    private String[] sectionCat;

    private String[] pageCat;

    private String page;

    private String ref;

    private String search;

    private Integer mobile;

    private Integer privacyPolicy;

    private Publisher publisher;

    private Content content;

    private String keywords;

}
