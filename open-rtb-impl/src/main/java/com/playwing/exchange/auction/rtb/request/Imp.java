package com.playwing.exchange.auction.rtb.request;

import com.playwing.exchange.auction.rtb.AbstractRTBObject;
import com.playwing.exchange.auction.rtb.request.imp.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Imp extends AbstractRTBObject{

 private String id;

 private Metric metric;

 private Banner banner;

 private Video video;

 private NativeAd nativeAd;

 private Audio audio;

 private Pmp pmp;

 private String displayManager;

 private String displayManageServer;

 private int instl;

 private String tagId;

 private Double bidFloor = 0d;

 private String bidFloorCur = "USD";

 private Integer clickBrowser;

 private Integer secure;

 private String[] iFrameBuster;

 private Integer exp;
}
