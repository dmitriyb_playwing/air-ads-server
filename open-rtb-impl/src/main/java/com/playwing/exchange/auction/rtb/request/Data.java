package com.playwing.exchange.auction.rtb.request;

import com.playwing.exchange.auction.rtb.AbstractRTBObject;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Data extends AbstractRTBObject{

    private String id;

    private String name;

    private List<Segment> segmentList;

}
