package com.playwing.exchange.auction.rtb.request;

import com.playwing.exchange.auction.rtb.AbstractRTBObject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Geo extends AbstractRTBObject{

    private Double lat;

    private Double lon;

    private Integer type;

    private Integer accuracy;

    private Integer lastfix;

    private Integer ipService;

    private String country;

    private String region;

    private String regionFips104;

    private String metro;

    private String city;

    private String zip;

    private Integer utCoffSet;
}
