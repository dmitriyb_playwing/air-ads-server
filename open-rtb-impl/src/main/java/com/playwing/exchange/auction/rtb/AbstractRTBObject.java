package com.playwing.exchange.auction.rtb;

import com.playwing.exchange.auction.rtb.request.Ext;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class AbstractRTBObject {

    private Ext ext;
}
