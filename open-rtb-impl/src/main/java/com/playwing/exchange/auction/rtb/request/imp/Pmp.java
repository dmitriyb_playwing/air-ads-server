package com.playwing.exchange.auction.rtb.request.imp;

import com.playwing.exchange.auction.rtb.AbstractRTBObject;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Pmp extends AbstractRTBObject{

    private int privateAuction;

    private List<Deal> deals;

}
