package com.playwing.exchange.auction.rtb.request;

import com.playwing.exchange.auction.rtb.AbstractRTBObject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Device extends AbstractRTBObject{

    private String ua;

    private Geo geo;

    private Integer dnt;

    private Integer lmt;

    private String ip;

    private String ipv6;

    private Integer deviceType;

    private String make;

    private String model;

    private String os;

    private String osv;

    private String hwv;

    private Integer h;

    private Integer w;

    private Integer ppi;

    private Double pxratio;

    private Integer js;

    private Integer geoFetch;

    private String flashVer;

    private String language;

    private String carrier;

    private String mccmnc;

    private Integer connectionType;

    private String ifa;

    private String didsha1;

    private String didm5;

    private String dpidsha1;

    private String macsha1;

    private String macmd5;

}
