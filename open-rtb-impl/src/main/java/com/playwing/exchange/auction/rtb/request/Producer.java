package com.playwing.exchange.auction.rtb.request;

import com.playwing.exchange.auction.rtb.AbstractRTBObject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Producer extends AbstractRTBObject{

    private String id;

    private String name;

    private String[] cat;

    private String domain;

}
