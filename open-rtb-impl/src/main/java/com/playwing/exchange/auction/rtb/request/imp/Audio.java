package com.playwing.exchange.auction.rtb.request.imp;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Audio extends AbstractDurableRTBFormat {

    private Integer maxSeq;

    private Integer feed;

    private Integer stitched;

    private Integer nvol;
}
