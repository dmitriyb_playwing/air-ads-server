package com.playwing.exchange.auction.rtb;

import com.playwing.exchange.auction.rtb.response.SeatBid;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class BidResponse extends AbstractRTBObject {

    private String id;

    private List<SeatBid> seatBidList;

    private String bidId;

    private String cur = "USD";

    private String customData;

    private Integer nbr;
}
