package com.playwing.exchange.auction.rtb.request.imp;

import com.playwing.exchange.auction.rtb.AbstractRTBObject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Deal extends AbstractRTBObject{

    private String id;

    private Double bidFloor = 0d;

    private String bidFloorCut = "USD";

    private Integer at;

    private String[] wSeat;

    private String[] waDomain;

}

