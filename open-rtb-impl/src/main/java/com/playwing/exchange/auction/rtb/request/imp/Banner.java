package com.playwing.exchange.auction.rtb.request.imp;

import com.playwing.exchange.auction.rtb.AbstractRTBObject;
import com.playwing.exchange.auction.rtb.request.Ext;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Banner extends AbstractRTBObject{

    private Format[] formats;

    private Integer w;

    private Integer h;

    private Integer[] btype;

    private Integer[] battr;

    private Integer pos;

    private String mimes;

    private Integer topframe;

    private Integer[] expdir;

    private Integer[] api;

    private String id;

    private Integer vcm;

    @Deprecated
    private Integer wmax;

    @Deprecated
    private Integer hmax;

    @Deprecated
    private Integer wmin;

    @Deprecated
    private Integer hmin;

}
