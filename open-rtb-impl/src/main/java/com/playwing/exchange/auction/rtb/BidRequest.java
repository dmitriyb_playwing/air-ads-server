package com.playwing.exchange.auction.rtb;

import com.playwing.exchange.auction.rtb.request.*;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

// Fields description is available at the OpenRTB specification.

@Getter
@Setter
public class BidRequest extends AbstractRTBObject{

	private String id;

	private List<Imp> imps = new ArrayList<>();

	private Site site;

	private App app;

	private Device device;

	private User user;

	private int test;

	private int at = 2;

	private Integer tmax;

	private String[] wseat;

	private String[] bseat;

	private int allimps;

	private String[] cur;

	private String[] wlang;

	private String[] bcat;

	private String[] badv;

	private String[] bapp;

	private Source source;

	private Regs regs;
}