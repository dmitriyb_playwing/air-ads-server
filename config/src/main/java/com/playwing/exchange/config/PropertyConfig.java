package com.playwing.exchange.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@Configuration
@PropertySource({"classpath:common.properties"})
public class PropertyConfig {

	@Configuration
	@Profile("test")
	@PropertySource("classpath:env-dev.properties")
	static class Test {
	}

	/**
	 * Properties to support the 'test' mode of operation.
	 */
	@Configuration
	@Profile({"dev"})
	@PropertySource("classpath:env-dev.properties")
	static class Dev {
	}

	/**
	 * Properties to support the 'production' mode of operation.
	 */
	@Configuration
	@Profile("production")
	@PropertySources({
			@PropertySource(value = "classpath:env-production.properties", ignoreResourceNotFound = true),
			@PropertySource(value = "file:${MEDIATOR_CONF_DIR}/env-production.properties", ignoreResourceNotFound = true)})
	static class Production {
		// Define additional beans for this profile here
	}
}
