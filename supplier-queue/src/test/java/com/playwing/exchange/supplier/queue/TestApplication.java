package com.playwing.exchange.supplier.queue;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication(scanBasePackages = {"com.playwing.exchange.supplier"})
public class TestApplication extends SpringBootServletInitializer {
}
