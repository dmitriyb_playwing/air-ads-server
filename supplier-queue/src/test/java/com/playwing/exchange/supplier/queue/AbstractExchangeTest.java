package com.playwing.exchange.supplier.queue;

import com.playwing.exchange.supplier.queue.service.SupplierQueueServiceImpl;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TestApplication.class, SupplierQueueServiceImpl.class})
@DataJpaTest
public abstract class AbstractExchangeTest {
}
