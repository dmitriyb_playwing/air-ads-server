package com.playwing.exchange.supplier.queue;

import com.playwing.exchange.supplier.queue.entity.SupplierData;
import com.playwing.exchange.supplier.queue.entity.SupplierPosition;
import com.playwing.exchange.supplier.queue.repository.SuppliersRepository;
import com.playwing.exchange.supplier.queue.service.SupplierQueueService;
import org.junit.After;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class SupplierQueueServiceTest extends AbstractExchangeTest {

	@Autowired
	private SupplierQueueService supplierQueueService;

	@Autowired
	private SuppliersRepository suppliersRepository;

	@After
	public void tearDown() {
		suppliersRepository.deleteAll();
	}

	@Test
	public void shouldTestSorting() {
		SupplierPosition supplierPosition1 = new SupplierPosition();
		supplierPosition1.setPosition(2);
		SupplierData supplierData = new SupplierData();
		supplierData.setName("first");
		supplierPosition1.setSupplierData(supplierData);
		SupplierData supplierData2 = new SupplierData();
		supplierData.setName("second");
		SupplierPosition supplierPosition2 = new SupplierPosition();
		supplierPosition2.setPosition(1);
		supplierPosition2.setSupplierData(supplierData2);
		suppliersRepository.save(supplierPosition1);
		suppliersRepository.save(supplierPosition2);
		List<SupplierPosition> supplierDataList = supplierQueueService.getFullSupplierDataList();
		assertEquals(1, supplierDataList.get(0).getPosition().intValue());
		assertEquals(2, supplierDataList.get(1).getPosition().intValue());
	}
}
