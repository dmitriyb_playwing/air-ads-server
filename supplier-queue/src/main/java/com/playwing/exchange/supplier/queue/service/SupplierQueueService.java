package com.playwing.exchange.supplier.queue.service;

import com.playwing.exchange.supplier.queue.entity.SupplierPosition;

import java.util.List;

public interface SupplierQueueService {

	List<SupplierPosition> getFullSupplierDataList();

	List<SupplierPosition> getSupplierDataList(int position);

}
