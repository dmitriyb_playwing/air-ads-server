package com.playwing.exchange.supplier.queue.repository;

import com.playwing.exchange.supplier.queue.entity.SupplierPosition;
import org.springframework.data.repository.CrudRepository;

public interface SuppliersRepository extends CrudRepository<SupplierPosition, Long> {
}
