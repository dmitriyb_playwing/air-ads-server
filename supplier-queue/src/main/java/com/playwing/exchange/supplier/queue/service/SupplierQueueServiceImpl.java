package com.playwing.exchange.supplier.queue.service;

import com.google.common.collect.Lists;
import com.playwing.exchange.supplier.queue.entity.SupplierPosition;
import com.playwing.exchange.supplier.queue.repository.SuppliersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SupplierQueueServiceImpl implements SupplierQueueService {

	@Autowired
	private SuppliersRepository suppliersRepository;

	@Override
	public List<SupplierPosition> getFullSupplierDataList() {
		ArrayList<SupplierPosition> supplierPositions = Lists.newArrayList(suppliersRepository.findAll());
		supplierPositions.sort(Comparator.comparing(SupplierPosition::getPosition));
		return supplierPositions;
	}

	@Override
	public List<SupplierPosition> getSupplierDataList(int position) {
		ArrayList<SupplierPosition> supplierPositions = Lists.newArrayList(suppliersRepository.findAll());
		return supplierPositions.stream().filter(sp -> sp.getPosition() == position).collect(Collectors.toList());
	}
}
