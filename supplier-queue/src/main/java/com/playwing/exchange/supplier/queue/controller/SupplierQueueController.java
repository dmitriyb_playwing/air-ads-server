package com.playwing.exchange.supplier.queue.controller;

import com.playwing.exchange.supplier.queue.entity.SupplierPosition;
import com.playwing.exchange.supplier.queue.service.SupplierQueueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController("/api/exchange/suppliers-queue")
public class SupplierQueueController {

	@Autowired
	private SupplierQueueService supplierQueueService;

	@GetMapping("/showQueue")
	public ResponseEntity<List<SupplierPosition>> getAllSuppliers() {
		return new ResponseEntity<>(supplierQueueService.getFullSupplierDataList(), HttpStatus.OK);
	}
}
