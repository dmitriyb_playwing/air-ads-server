package com.playwing.exchange.decision.engine.service;

import com.playwing.exchange.decision.engine.entity.AdRequest;
import com.playwing.exchange.decision.engine.entity.AdResponse;
import com.playwing.exchange.decision.engine.supplier.AdSupplier;
import com.playwing.exchange.decision.engine.supplier.SupplierQueueManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DecisionServiceImpl implements DecisionService {

	@Autowired
	private SupplierQueueManager supplierQueueManager;

	public AdResponse getAd(AdRequest adRequest, Map<String, String> httpRequest) {
		log.info("Getting ads for appId " + adRequest.getApp().getAppId());
		AdResponse result = null;
		int queueLength = supplierQueueManager.getQueueLength();
		for (int i = 0; i < queueLength; i++) {
			result = executeAndGetAd(supplierQueueManager.getSuppliers(i), adRequest, httpRequest);
		}


		return result;
	}

	private AdResponse executeAndGetAd(List<AdSupplier> adSuppliers, AdRequest adRequest, Map<String, String> httpRequest) {
		List<CompletableFuture<AdResponse>> futures = new ArrayList<>();
		for (AdSupplier adSupplier : adSuppliers) {
			futures.add(adSupplier.getAd(adRequest, httpRequest));
		}
		CompletableFuture<AdResponse>[] ads = new CompletableFuture[]{};
		CompletableFuture<AdResponse>[] completableFutures = futures.toArray(ads);
		CompletableFuture.allOf(completableFutures).join();
		List<AdResponse> collect = Arrays.stream(completableFutures).map(ad -> {
			try {
				return ad.get();
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
			return null;
		}).collect(Collectors.toList());
		return makeChoice(collect);
	}

	private AdResponse makeChoice(List<AdResponse> collect) {
		return collect.get(0);
	}
}
