package com.playwing.exchange.decision.engine.entity.request;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Content {

	private String kws;

}
