package com.playwing.exchange.decision.engine.exception;

public abstract class XMXException extends RuntimeException {
	public XMXException() {

	}

	public XMXException(String text) {
		super(text);
	}
}
