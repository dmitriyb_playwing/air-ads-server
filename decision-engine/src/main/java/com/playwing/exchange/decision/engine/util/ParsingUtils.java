package com.playwing.exchange.decision.engine.util;

import com.playwing.exchange.decision.engine.dto.AdResponseDto;
import com.playwing.exchange.decision.engine.exception.TechnicalException;
import lombok.extern.slf4j.Slf4j;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;

@Slf4j
public final class ParsingUtils {

	private ParsingUtils() {
	}

	public static void fromXmlToString(String xml, AdResponseDto smaatoAdResponseDto) {
		try {
			StringReader sr = new StringReader(xml);
			JAXBContext jaxbContext = JAXBContext.newInstance(AdResponseDto.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			smaatoAdResponseDto = (AdResponseDto) unmarshaller.unmarshal(sr);
		} catch (JAXBException e) {
			log.error("Can't parse response", e);
			throw new TechnicalException("Can't parse smaato response");
		}

	}
}
