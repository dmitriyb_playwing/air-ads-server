package com.playwing.exchange.decision.engine.entity.request;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Location {

	private String gps;

	private String region;

	private String zip;

}
