package com.playwing.exchange.decision.engine.supplier.smaato;

public class SmaatoConstants {

	/**
	 * response yes Desired format of the response. xml, html or json. (must be XML for Video). string html
	 */
	public static final String RESPONSE_FORMAT = "response";

	public static final String API_VERSION = "apiver"; // integer 502
	public static final String AD_UNIT = "adspace"; // integer 123456789
	public static final String PUBLISHER_ID = "pub"; // integer 4711
	public static final String DEVICE_IP = "devip"; // Required if not forwarding the headers with proper prefix.

	/**
	 * coppa yes (traffic from the US) "0" will indicate that your content should not be treated as child-directed for
	 * purposes of COPPA. "1" will indicate that your content should be treated as child-directed for purposes of COPPA.
	 * 0 or 1 0
	 */
	public static final String COPPA = "coppa";

	/**
	 * nsupport no, The requested native ad component names separated by comma. If not set, at least a title and a click
	 * through url will be included in the ad response. String: Comma separated values. title, txt, icon, image, ctatext
	 */
	public static final String NATIVE_SUPPORT = "nsupport";

	/**
	 * vastver Yes (if format=video) Set to 2 integer 2
	 */
	public static final String VAST_VERSION = "vastver";

	/**
	 * nver Yes (if format=native) Set to 1 integer 1
	 */
	public static final String NATIVE_VERSION = "nver";

	/**
	 * videotype Yes (if format=video) Choose between: instream-pre instream-mid instream-post outstream interstitial
	 * rewarded string instream-pre
	 */
	public static final String VIDEO_TYPE = "videotype";

	/**
	 * format yes The format of the ad to be returned: all: Image, text or rich media (strongly recommended!) img: Any
	 * image format txt: Text richmedia: Rich media video: Video Ads (Note: "response" must be "XML") native: Native Ads
	 * (Note: "response" must be "XML") string img
	 */
	public static final String AD_FORMAT = "format";

	/**
	 * formatstrict no Default: false. When the format parameter is handled as "strict" only the desired format will be
	 * delivered. If it's not strict, an alternative format might be delivered, if the desired format is not available.
	 * boolean true
	 */
	public static final String AD_FORMAT_STRICT = "formatstrict";

	/**
	 * dimension no The desired dimension of ad to be returned: xxlarge (recommended): this is the most common size (320
	 * x 50) medrect: IAB Medium Rectangle (300 x 250) sky: IAB Skyscraper (120 x 600) leader: IAB Leaderboard (728 x
	 * 90) full_320x480: Interstitial ad for smartphones (portrait - switch dimensions for landscape) full_640x960:
	 * Interstitial ad for tablets (portrait - switch dimensions for landscape) full_640x1136: Interstitial ad for
	 * tablets (portrait - switch dimensions for landscape) full_768x1024: Interstitial ad for tablets (portrait -
	 * switch dimensions for landscape) full_800x1280: Interstitial ad for tablets (portrait - switch dimensions for
	 * landscape) string xxlarge
	 */
	public static final String AD_DIMENSION = "dimension";

	/**
	 * dimensionstrict no Default: false. When the dimension parameter is handled as "strict" only the desired dimension
	 * will be delivered. If it's not strict, an alternative dimension might be delivered, if the desired format is not
	 * available. boolean false
	 */
	public static final String AD_DIMENSION_STRICT = "dimensionstrict";

	/**
	 * carrier no Name of the current carrier. string o2uk
	 */
	public static final String CARRIER = "carrier";

	/**
	 * carriercode no MCC and MNC code of the current carrier. Format: carriercode={mcc}{mnc}. In the example the MCC is
	 * 234 and the MNC is 02. integer 23402
	 */
	public static final String CARRIER_CODE = "carriercode";

	/**
	 * height no Height of the ad space. Recommended for in-application advertising. integer 36
	 */
	public static final String AD_UNIT_HEIGHT = "height";

	/**
	 * width no Width of the ad space. Recommended for applications. integer 36
	 */
	public static final String AD_UNIT_WIDTH = "width";

	/**
	 * session no A MD5 hash generated at the start of every session and used throughout the whole session. We recommend
	 * hashing the device's hardware ID and the time of session start. mD5 c30f5ddc0be5031ddbdcc962cce07ac9
	 */
	public static final String SESSION = "session";

	// DEVICE TRACKING

	/**
	 * iosadid yes (for iOS applications) Apple's Advertising Identifier. See here string
	 * 1D76F5D1-1983-47C8-B18D-119D52E4597A
	 */
	public static final String IOS_AD_ID = "iosadid";

	/**
	 * iosadtracking yes (for iOS applications) Apple's advertisingTrackingEnabled Property. See here. (false = user has
	 * decided against tracking - this is the opposite way around as on Android) boolean true
	 */
	public static final String IOS_AD_TRACKING = "iosadtracking";

	/**
	 * googleadid yes (for Android Applications) Google Android Advertising ID. See http://bit.ly/MBMTTJ string
	 * 40d9e394-740a-4225-83f1-766a04154b83
	 */
	public static final String GOOGLE_AD_ID = "googleadid";

	/**
	 * googlednt yes (for Android Applications) Android limit ad tracking preference. (true = user has decided against
	 * tracking - this is the opposite way around as on iOS) boolean true
	 */
	public static final String GOOGLE_AD_TRACKING = "googlednt";

	/**
	 * androidid no The Android device's Android ID (Settings.Secure.ANDROID_ID) string 9774d56d682e549c
	 */
	public static final String ANDROID_ID = "androidid";
	/**
	 * bbid yes (for BlackBerry Applications) The BlackBerry Device ID string e3365064be00b474622467f06493a2d282a464ad
	 */
	public static final String BLACKBERRY_ID = "bbid";
	/**
	 * wpid yes (for Windows Phone Applications) The Windows Phone Device ID string 513339592119231246
	 */
	public static final String WINDOWS_PHONE_ID = "wpid";

	// NON Mandatory if fowarding proper headers
	/**
	 * divid yes (for mobile web) Mobile Web only: The ID of the div (or equally suitable HTML element) that you're
	 * using to serve the ad. This needs to be formatted in the following way: smt-[AdspaceID] Please note: If you fail
	 * to do this, ad display may not work. string smt-123456789
	 */
	public static final String DIV_ID = "divid";

	/**
	 * ref yes ('no' if the device is requesting directly or if you're sending the correct referer header of the
	 * requesting webpage) Referer: The URL that identifies the address of the webpage where the ad request is coming
	 * from. string http%3A%2F%2Fwww.example.com
	 */
	public static final String REF = "ref";

	/**
	 * device yes ('no' if the device user agent is sent as a header) The user agent of the device's default string
	 * Mozilla%2F5.0%20(Linux%3B%2 0U%3B%20Android%202.2.1%3B% 20en-us%3B%20Nexus%20One%20 Build%2FFRG83)
	 * %20AppleWebKit%2F533.1%20(K HTML%2C%20like%20Gecko)%20V ersion%2F4.0%20Mobile%20Saf ari%2F533.1
	 */
	public static final String DEVICE_USER_AGENT = "device";

	/**
	 * mraidver 1 - MRAID 1.x 2 - MRAID 2.x integer 2
	 */
	public static final String MRAID_VERSION = "mraidver";

	// ADDITIONAL TARGETTING PARAMETERS

	/**
	 * Tags (free text, case insensitive) describing the content.
	 */
	public static final String KEYWORDS = "kws"; // motorsport,news,cars

	/**
	 * The user's age. 2-digit number. If only a range is available, use the mean average.
	 */
	public static final String USER_AGE = "age"; // 30

	public static final String USER_GENDER = "gender"; // m,f

	/**
	 * Query String: A search term entered by the user within the mobile site.
	 */
	public static final String USER_QUERY_STRING = "qs"; // coffee, san+francisco

	// LOCATION
	/**
	 * GPS coordinates of the user's location. string: Latitude and longitude in decimal degrees format, comma separated
	 */
	public static final String GPS = "gps"; // 37.530676,-122.262447

	/**
	 * The region or state, the user is located in.
	 */
	public static final String REGION = "region"; // string california
	public static final String ZIP = "zip"; // string 94402

	// DEVICE
	public static final String DEVICE_MODEL = "devicemodel"; // The device model. String iPhone3,1
	public static final String DEVICE_MAKE = "devicemake"; // The device manufacturer. string Apple

	/**
	 * The current location area code (LAC). string +40
	 */
	public static final String AREA_CODE = "areacode";
	public static final String AD_SPACE = "adspace";
}
