package com.playwing.exchange.decision.engine.supplier.smaato;

import com.google.common.collect.Lists;
import com.playwing.exchange.decision.engine.converters.SmaatoConverter;
import com.playwing.exchange.decision.engine.entity.AdRequest;
import com.playwing.exchange.decision.engine.entity.AdResponse;
import com.playwing.exchange.decision.engine.entity.response.AdContent;
import com.playwing.exchange.decision.engine.entity.response.ResponseTag;
import com.playwing.exchange.decision.engine.entity.response.adcontent.Rtb;
import com.playwing.exchange.decision.engine.entity.response.adcontent.type.Video;
import com.playwing.exchange.decision.engine.supplier.RestAdSupplier;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@Service("smaatoAdSupplier")
@Slf4j
public class SmaatoRestAdSupplierImpl extends RestAdSupplier implements SmaatoRestAdSupplier {

	private static final String URI = "http://soma.smaato.net/oapi/reqAd.jsp";

	@Override
	@Async
	public CompletableFuture<AdResponse> getAd(AdRequest adRequest, Map<String, String> httpRequest) {
		HttpHeaders headers = prepareRequest(httpRequest);
		AdResponse adResponse = sendRequest(headers, adRequest);
		return CompletableFuture.completedFuture(adResponse);
	}

	private HttpHeaders prepareRequest(Map<String, String> httpRequest) {
		HttpHeaders headers = new HttpHeaders();
		for (String key : httpRequest.keySet()) {
			headers.add("x-mh-" + key, httpRequest.get(key));
		}
		return headers;
	}

	private AdResponse sendRequest(HttpHeaders headers, AdRequest adRequest) {
		List<NameValuePair> requestParams = new ArrayList<>();
		new SmaatoConverter().toRequestParams(adRequest, requestParams);
		String paramsString = URLEncodedUtils.format(requestParams, "UTF-8");
		String uri = URI + "?" + paramsString;
		ResponseEntity<String> responseEntity = restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<>("parameters", headers), String.class);

		return adaptResponse(responseEntity.getBody());
	}

	private AdResponse adaptResponse(String response) {
		AdResponse adResponse = new AdResponse();
		ResponseTag responseTag = new ResponseTag();
		responseTag.setTagId(9924002);
		responseTag.setAuctionId("3309462560570472492");
		responseTag.setNobid(false);
		responseTag.setNoAdUrl("mock_no_ad_url");
		responseTag.setTimeoutMs(1000);
		responseTag.setAdProfileId(1003565);
		AdContent adContent = new AdContent();
		adContent.setContentSource("rtb");
		adContent.setAdType("video");
		adContent.setNotifyUrl("mock_notify_url");
		adContent.setUsersyncUrl("mock_usersync_url");
		adContent.setBuyerMembderId(958);
		adContent.setCreativeId(57584903);
		adContent.setMediaSubtypeId(64);
		adContent.setClientInitiatedAdCounting(true);
		responseTag.setAds(Lists.newArrayList(adContent));
		Rtb rtb = new Rtb();
		Video video = new Video();
		video.setDurationMs(96000L);
		video.setContent(response);
		video.setFrameworks(Lists.newArrayList("vpaid_2_0"));
		video.setPlaybackMethods(Lists.newArrayList("unknown"));
		rtb.setVideo(video);
		adContent.setRtb(rtb);
		adResponse.addToTags(responseTag);
		adResponse.setVersion("0.0.1");
		return adResponse;
	}

}
