package com.playwing.exchange.decision.engine.supplier;

import com.playwing.exchange.decision.engine.entity.AdRequest;
import com.playwing.exchange.decision.engine.entity.AdResponse;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

public interface AdSupplier {

	CompletableFuture<AdResponse> getAd(AdRequest adRequest, Map<String, String> httpRequest);

}
