package com.playwing.exchange.decision.engine.converters;

import com.playwing.exchange.decision.engine.entity.AdRequest;
import com.playwing.exchange.decision.engine.entity.request.Tag;
import com.playwing.exchange.decision.engine.enums.AllowedAdType;
import com.playwing.exchange.decision.engine.exception.AdFormatsAreMissingException;
import com.playwing.exchange.decision.engine.supplier.smaato.SmaatoConstants;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.List;

public class SmaatoConverter {

	//TODO: Remove hardcode
	private static final transient String API_VERSION = "502";
	private static final String TEST_SPACE = "3090";
	private static final String TEST_PUBLISHER = "0";
	private static final String VERSION = "0.0.1";

	public void toRequestParams(AdRequest source, List<NameValuePair> destination) {
		//Tag will be the only one, until multi-call feature is requested.
		Tag tag = source.getTags().get(0);
		destination.add(new BasicNameValuePair(SmaatoConstants.API_VERSION, API_VERSION));
		destination.add(new BasicNameValuePair(SmaatoConstants.AD_SPACE, TEST_SPACE));
		destination.add(new BasicNameValuePair(SmaatoConstants.PUBLISHER_ID, TEST_PUBLISHER));
		if (tag.getAllowedMediaTypes() == null || tag.getAllowedMediaTypes().isEmpty()) {
			throw new AdFormatsAreMissingException();
		}

		/*if (source.getDevice().getOs().contains("Android")) {
			destination.add(new BasicNameValuePair(SmaatoConstants.GOOGLE_AD_ID, source.getDevice().getDeviceId().getAaid()));
			destination.add(new BasicNameValuePair(SmaatoConstants.GOOGLE_AD_TRACKING, String.valueOf(source.getDevice().getLimitAdTracking())));
		} else {
			destination.add(new BasicNameValuePair(SmaatoConstants.IOS_AD_ID, source.getDevice().getDeviceId().getIdfa()));
			destination.add(new BasicNameValuePair(SmaatoConstants.IOS_AD_TRACKING, String.valueOf(source.getDevice().getLimitAdTracking())));
		}*/

		if (tag.getAllowedMediaTypes().stream().anyMatch(i -> i == AllowedAdType.VIDEO.getId() || i == AllowedAdType.INTERSTITIAL.getId() || i == AllowedAdType.REWARDED_VIDEO.getId())) {
			fillForVideo(tag, destination);
		}

		destination.add(new BasicNameValuePair(SmaatoConstants.COPPA, String.valueOf(1)));
	}

	private void fillForVideo(Tag tag, List<NameValuePair> destination) {
		destination.add(new BasicNameValuePair(SmaatoConstants.AD_FORMAT, "video"));
		destination.add(new BasicNameValuePair(SmaatoConstants.RESPONSE_FORMAT, "xml"));
		destination.add(new BasicNameValuePair(SmaatoConstants.VIDEO_TYPE, "outstream"));
		destination.add(new BasicNameValuePair(SmaatoConstants.VAST_VERSION, "2"));
		//destination.add(new BasicNameValuePair(SmaatoConstants.AD_UNIT_HEIGHT);
		//destination.add(new BasicNameValuePair((SmaatoConstants.AD_UNIT_WIDTH)));
	}


}
