package com.playwing.exchange.decision.engine.service;

import com.playwing.exchange.decision.engine.entity.AdRequest;
import com.playwing.exchange.decision.engine.entity.AdResponse;

import java.util.Map;

public interface DecisionService {

	AdResponse getAd(AdRequest adRequest, Map<String, String> httpRequest);
}
