package com.playwing.exchange.decision.engine.exception;

public class RequiredFieldIsMissingException extends BusinesException {
	public RequiredFieldIsMissingException(String text) {
		super("Required field " + text + " is missing in the request");
	}
}
