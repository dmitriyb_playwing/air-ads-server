package com.playwing.exchange.decision.engine.exception;

public class AdFormatsAreMissingException extends BusinesException {
	//TODO: Implement loading exception text from file, using error codes;
	public AdFormatsAreMissingException() {
		super("AdFormats are missing");
	}
}
