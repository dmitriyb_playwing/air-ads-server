package com.playwing.exchange.decision.engine.entity.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.playwing.exchange.decision.engine.entity.request.device.DeviceId;
import com.playwing.exchange.decision.engine.entity.request.device.Geo;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Device {

	@JsonProperty("useragent")
	private String userAgent;

	private Geo geo;

	private String make;

	private String model;

	private String orientation;

	private String os;

	private String carrier;

	@JsonProperty("connectiontype")
	private String connectionType;

	private Integer mcc;

	private Integer mnc;

	@JsonProperty("limit_ad_tracking")
	private Boolean limitAdTracking;

	@JsonProperty("device_id")
	private DeviceId deviceId;

	@JsonProperty("devtime")
	private Long devTime;

	@JsonProperty("devtz")
	private String devTz;

}
