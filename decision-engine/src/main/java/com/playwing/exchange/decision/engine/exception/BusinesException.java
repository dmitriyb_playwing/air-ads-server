package com.playwing.exchange.decision.engine.exception;

public class BusinesException extends XMXException {
	public BusinesException(String text) {
		super(text);
	}
}
