package com.playwing.exchange.decision.engine.exception;

public class TechnicalException extends XMXException {
	public TechnicalException(String s) {
		super(s);
	}
}
