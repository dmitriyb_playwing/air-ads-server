package com.playwing.exchange.decision.engine.enums;

public enum AllowedAdType {

	BANNER(1), INTERSTITIAL(3), VIDEO(4), REWARDED_VIDEO(8), NATIVE(12);

	private int id;

	AllowedAdType(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

}
