package com.playwing.exchange.decision.engine.entity.request.tag;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Video {

	private Integer minDuration;

	private Integer maxDuration;

}
