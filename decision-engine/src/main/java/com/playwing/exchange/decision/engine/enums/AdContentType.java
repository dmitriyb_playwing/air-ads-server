package com.playwing.exchange.decision.engine.enums;

public enum AdContentType {
	HTML, // Simple HTML ads.
	MRAID, // MRAID Compliant HTML AdContent.
	VAST, // VAST Compliant XML adContent.
	NATIVE, // NativeAd.
	CUSTOM // Same as native but support for richmedia urls Eg. x-mediate://expand
}
