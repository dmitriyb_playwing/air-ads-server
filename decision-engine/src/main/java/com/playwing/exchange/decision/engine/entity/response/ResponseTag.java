package com.playwing.exchange.decision.engine.entity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ResponseTag {

	@JsonProperty("tag_id")
	private Integer tagId;

	@JsonProperty("auction_id")
	private String auctionId;

	private boolean nobid;

	@JsonProperty("no_ad_url")
	private String noAdUrl;

	@JsonProperty("timeout_ms")
	private Integer timeoutMs;

	@JsonProperty("ad_profile_id")
	private Integer adProfileId;

	private List<AdContent> ads;
}
