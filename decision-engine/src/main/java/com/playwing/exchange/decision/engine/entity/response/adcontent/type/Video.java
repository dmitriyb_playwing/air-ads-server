package com.playwing.exchange.decision.engine.entity.response.adcontent.type;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class Video implements AbsctractType {

	@JsonProperty("duration_ms")
	private Long durationMs;

	@JsonProperty("playback_methods")
	private List<String> playbackMethods;

	private List<String> frameworks;

	private String content;
}
