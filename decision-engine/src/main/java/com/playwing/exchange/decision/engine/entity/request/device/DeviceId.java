package com.playwing.exchange.decision.engine.entity.request.device;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DeviceId {

	private String aaid;

	private String idfa;

	private String md5udid;

	private String sha1udid;

	private String openudid;

	private String sha1mac;
}
