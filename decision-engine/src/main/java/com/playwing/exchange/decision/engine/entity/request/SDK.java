package com.playwing.exchange.decision.engine.entity.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SDK {

	private String source;

	private String version;
}
