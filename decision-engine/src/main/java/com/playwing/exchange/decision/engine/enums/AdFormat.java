package com.playwing.exchange.decision.engine.enums;

public enum AdFormat {

	BANNER(1, "Banner"), INTERSTITIAL(2, "Interstitial"), VIDEO(3, "Video"), NATIVE(4, "Native"), REWARDED_VIDEO(5, "Rewarded Video");

	private final Integer id;
	private final String label;

	AdFormat(Integer id, String label) {
		this.id = id;
		this.label = label;
	}

	public static AdFormat getByName(String name) throws Exception {
		for (AdFormat adFormat : AdFormat.values()) {
			if (adFormat.name().equals(name)) return adFormat;
		}
		throw new Exception("Invalid Ad Format name");
	}

	public static AdFormat getById(Integer id) {
		for (AdFormat adFormat : AdFormat.values()) {
			if (adFormat.getId().equals(id)) return adFormat;
		}
		return null;
	}

	public Integer getId() {
		return id;
	}

	public String getLabel() {
		return label;
	}
}
