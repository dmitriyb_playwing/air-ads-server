package com.playwing.exchange.decision.engine.entity;

import com.playwing.exchange.decision.engine.entity.response.ResponseTag;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class AdResponse {

	private String version;

	private List<ResponseTag> tags = new ArrayList<>();

	public void addToTags(ResponseTag responseTag) {
		tags.add(responseTag);
	}
}
