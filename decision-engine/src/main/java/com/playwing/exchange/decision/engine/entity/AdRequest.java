package com.playwing.exchange.decision.engine.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.playwing.exchange.decision.engine.entity.request.*;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.List;

@Getter
@Setter
public class AdRequest {

	private int member_id;

	private List<Tag> tags;

	private User user;

	private Device device;

	private App app;

	private String sdkver;

	private SDK sdk;

	@JsonProperty("supply_type")
	private String supplyType;

	private HashMap<String, String> keyWords;
}
