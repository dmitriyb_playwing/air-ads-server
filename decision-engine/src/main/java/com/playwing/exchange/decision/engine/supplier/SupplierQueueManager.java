package com.playwing.exchange.decision.engine.supplier;

import com.playwing.exchange.supplier.queue.entity.SupplierPosition;
import com.playwing.exchange.supplier.queue.service.SupplierQueueService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Log4j
public class SupplierQueueManager {

	@Autowired
	private SupplierQueueService supplierQueueService;

	@Autowired
	private ApplicationContext applicationContext;

	private List<SupplierPosition> suppliers = new ArrayList<>();

	@PostConstruct
	public void initSuppliers() {
		suppliers = supplierQueueService.getFullSupplierDataList();
	}

	public List<AdSupplier> getSuppliers(int position) {
		return suppliers.stream().filter(sp -> sp.getPosition() == position).map(sp -> (AdSupplier) applicationContext.getBean(sp.getSupplierData().getName())).collect(Collectors.toList());
	}

	public int getQueueLength() {
		return suppliers.get(suppliers.size() - 1).getPosition();
	}
}
