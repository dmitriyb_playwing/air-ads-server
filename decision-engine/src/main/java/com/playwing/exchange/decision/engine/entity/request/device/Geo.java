package com.playwing.exchange.decision.engine.entity.request.device;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Geo {

	private Double lat;

	private Double lng;

	private Integer loc_age;

	private Integer loc_precision;
}
