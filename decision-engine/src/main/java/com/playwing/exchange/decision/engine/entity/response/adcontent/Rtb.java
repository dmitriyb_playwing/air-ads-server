package com.playwing.exchange.decision.engine.entity.response.adcontent;

import com.playwing.exchange.decision.engine.entity.response.adcontent.type.AbsctractType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Rtb {

	private AbsctractType video;
}
