package com.playwing.exchange.decision.engine.supplier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

public abstract class RestAdSupplier {
	@Autowired
	protected RestTemplate restTemplate;
}
