package com.playwing.exchange.decision.engine.entity.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.playwing.exchange.decision.engine.entity.request.tag.Size;
import com.playwing.exchange.decision.engine.entity.request.tag.Video;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Tag {

	private String code;

	private Integer id;

	private List<Size> sizes;

	@JsonProperty("allowed_smaller_sizes")
	private boolean allowSmallerSizes;

	@JsonProperty("allowed_media_types")
	private List<Integer> allowedMediaTypes;

	private Video video;

	private boolean prebid;

	private Double reserve;

	@JsonProperty("disable_psa")
	private Boolean disablePsa;

	@JsonProperty("require_asset_url")
	private Boolean requireAssetUrl;

}
