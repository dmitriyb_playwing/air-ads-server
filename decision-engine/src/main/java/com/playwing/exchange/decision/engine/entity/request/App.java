package com.playwing.exchange.decision.engine.entity.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class App {

	@JsonProperty("appid")
	private String appId;
}
