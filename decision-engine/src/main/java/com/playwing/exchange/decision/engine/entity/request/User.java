package com.playwing.exchange.decision.engine.entity.request;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class User {

	private Integer age;

	private String gender;

	/**
	 * Query String: A search term entered by the user within the mobile site.
	 * Example: coffee, san+francisco
	 */
	private String language;
}
