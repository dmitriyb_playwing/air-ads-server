package com.playwing.exchange.decision.engine.entity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.playwing.exchange.decision.engine.entity.response.adcontent.Rtb;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdContent {

	@JsonProperty("content_source")
	private String contentSource;

	@JsonProperty("ad_type")
	private String adType;

	@JsonProperty("notify_url")
	private String notifyUrl;

	@JsonProperty("usersync_url")
	private String usersyncUrl;

	@JsonProperty("buyer_member_id")
	private Integer buyerMembderId;

	@JsonProperty("creative_id")
	private Integer creativeId;

	@JsonProperty("media_type_id")
	private Integer mediaTypeId;

	@JsonProperty("media_subtype_id")
	private Integer mediaSubtypeId;

	@JsonProperty("client_initiated_ad_counting")
	private Boolean clientInitiatedAdCounting;

	private Rtb rtb;
}
