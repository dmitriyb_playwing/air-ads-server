package com.playwing.exchange.decision.engine.supplier.smaato;

public enum SmaatoVideoTypesEnum {

	INSTREAM_PRE("instream-pre"), INSTREAM_MID("instream-mid"), INSTREAM_POST("instream-post"), OUTSTREAM("outstream"), INTERSTITIAL("interstitial"), REWARDED("rewarded");

	private String type;

	SmaatoVideoTypesEnum(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
}

