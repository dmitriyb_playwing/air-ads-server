package com.playwing.exchange.decision.engine.entity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.playwing.exchange.decision.engine.entity.request.Device;
import com.playwing.exchange.decision.engine.entity.request.Tag;
import com.playwing.exchange.decision.engine.entity.request.device.DeviceId;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class AdRequestTest {

	@Test
	public void shouldTest() throws JsonProcessingException {
		AdRequest adRequest = new AdRequest();
		List<Tag> tags = new ArrayList<>();
		Tag tag = new Tag();
		tag.setAllowedMediaTypes(new ArrayList<>());
		tag.setSizes(new ArrayList<>());
		tags.add(tag);
		adRequest.setTags(tags);
		Device device = new Device();
		device.setOs("Android");
		DeviceId deviceId = new DeviceId();
		deviceId.setAaid("test");
		device.setDeviceId(deviceId);
		adRequest.setDevice(device);
		ObjectMapper objectMapper = new ObjectMapper();
		String s = objectMapper.writeValueAsString(adRequest);
		System.out.println(s);

	}

}
