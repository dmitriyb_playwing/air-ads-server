package com.playwing.exchange.controllers;

import com.playwing.exchange.decision.engine.entity.AdRequest;
import com.playwing.exchange.decision.engine.entity.AdResponse;
import com.playwing.exchange.decision.engine.service.DecisionServiceImpl;
import com.playwing.exchange.util.HttpUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/exchange")
public class AdController {

	@Autowired
	private DecisionServiceImpl decisionService;

	@PostMapping(value = "/ad", produces = "application/json")
	public ResponseEntity<AdResponse> getAd(@RequestBody AdRequest adRequest, HttpServletRequest httpRequest) {
		AdResponse result = decisionService.getAd(adRequest, HttpUtils.getHeadersInfo(httpRequest));
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

}
