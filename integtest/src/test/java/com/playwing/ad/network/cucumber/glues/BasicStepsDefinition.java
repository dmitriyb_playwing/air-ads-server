package com.playwing.ad.network.cucumber.glues;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.Assert.assertTrue;

@ContextConfiguration
@SpringBootTest
public class BasicStepsDefinition {

	@When("app is loaded")
	public void appIsLoaded(){
	}

	@Then("all is fine")
	public void isFine(){
		assertTrue(true);
	}

}
