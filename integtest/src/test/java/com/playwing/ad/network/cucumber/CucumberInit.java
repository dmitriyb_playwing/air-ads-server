package com.playwing.ad.network.cucumber;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty"}, glue = "com.playwing.ad.network.cucumber.glues", features = "src/test/resources")
public class CucumberInit {
}
