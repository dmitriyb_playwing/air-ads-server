package com.playwing.playwing.exchange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlaywingAdNetworkApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlaywingAdNetworkApplication.class, args);
	}
}
